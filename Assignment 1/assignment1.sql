DROP TABLE Home CASCADE CONSTRAINTS;
DROP TABLE Occupation CASCADE CONSTRAINTS;
DROP TABLE Person CASCADE CONSTRAINTS;

CREATE TABLE Home (
    hid     CHAR(4)     PRIMARY KEY,
    address VARCHAR2(200)  NOT NULL
);

CREATE TABLE Occupation (
    oid     CHAR(4)         PRIMARY KEY,
    type    VARCHAR2(20)    NOT NULL,
    salary  NUMBER(10,2)    NOT NULL
);

CREATE TABLE Person (
    pid         CHAR(4)         PRIMARY KEY,
    firstName   VARCHAR2(20)    NOT NULL,
    lastName    VARCHAR2(20)    NOT NULL,
    father_id   CHAR(4)         REFERENCES Person(pid),
    mother_id   CHAR(4)         REFERENCES Person(pid),
    hid         CHAR(4)         REFERENCES Home(hid)        NOT NULL,
    oid         CHAR(4)         REFERENCES Occupation(oid)   NOT NULL
);

--Adding 2 households
INSERT INTO Home VALUES('H001', '123 Easy St.');
INSERT INTO Home VALUES('H002', '56 Fake Ln.');

--Adding the Occupations
INSERT INTO Occupation VALUES('O000', 'N\A', '0');
INSERT INTO Occupation VALUES('O001', 'Student', '0');
INSERT INTO Occupation VALUES('O002', 'Doctor', '100000');
INSERT INTO Occupation VALUES('O003', 'Professor', '80000');

--Adding people
INSERT INTO Person VALUES('P001', 'Zachary', 'Aberny', NULL, NULL, 'H002', 'O000');
INSERT INTO Person VALUES('P002', 'Yanni', 'Aberny', NULL, NULL, 'H002', 'O000');
INSERT INTO Person VALUES('P003', 'Alice', 'Aberny', 'P001', 'P002', 'H001', 'O002');
INSERT INTO Person VALUES('P004', 'Bob', 'Bortelson', NULL, NULL, 'H001', 'O003');
INSERT INTO Person VALUES('P005', 'Carl', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');
INSERT INTO Person VALUES('P006', 'Denise', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');

--Testing
SELECT 
    gp.firstname 
FROM 
    person gp JOIN Person p 
    ON gp.pid = p.mother_id OR gp.pid = p.father_id 
    JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id 
WHERE
    c.firstname = 'Denise';
    
--Queries
1.  SELECT
        h.address, COUNT(pid)
    FROM
        Person p INNER JOIN Home h
        ON p.hid = h.hid
    GROUP BY
        address;