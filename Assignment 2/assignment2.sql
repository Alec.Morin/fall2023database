CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
        RETURN NUMBER;
END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn VARCHAR2)
    RETURN NUMBER IS
        base_price      NUMBER(5,2);
        book_discount   NUMBER(4,2);
        final_price     NUMBER(5,2);
        BEGIN
            SELECT
                discount INTO book_discount
            FROM
                books
            WHERE
                book_isbn = isbn;
            SELECT  
                retail INTO base_price
            FROM
                books
            WHERE
                book_isbn = isbn;
            IF book_discount IS NOT NULL THEN 
                final_price:=base_price-((book_discount/100)*base_price);
            ELSE
                final_price:=base_price;
            END IF;
            RETURN(final_price);
        END;
        
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
    RETURN NUMBER IS
        discounted_price    NUMBER(5,2);
        final_price         NUMBER(5,2);
        BEGIN
            discounted_price:=price_after_discount(book_isbn);
            final_price:=discounted_price*1.15;
            RETURN(final_price);
        END;
        
END book_store;
/

DECLARE
book_isbn   books.isbn%TYPE;
book_price  books.retail%TYPE;
BEGIN
    SELECT
        isbn INTO book_isbn
    FROM
        books
    WHERE
        Title='BUILDING A CAR WITH TOOTHPICKS';
    book_price:=book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.PUT_LINE(book_price);
END;
/

DECLARE
book_isbn   books.isbn%TYPE;
book_price  books.retail%TYPE;
BEGIN
    SELECT
        isbn INTO book_isbn
    FROM
        books
    WHERE
        Title='HOLY GRAIL OF ORACLE';
    book_price:=book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.PUT_LINE(book_price);
END;
/