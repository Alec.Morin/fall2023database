CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax(book_isbn VARCHAR2) RETURN NUMBER;
    TYPE customer_array_type IS VARRAY(100) OF NUMBER;
    FUNCTION book_purchasers(book_isbn VARCHAR2) RETURN customer_array_type;
    PROCEDURE show_purchases;
END book_store;

/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn VARCHAR2) 
        RETURN NUMBER
        IS price NUMBER;
            book_discount NUMBER;
            new_price NUMBER;
        BEGIN
            SELECT retail, discount INTO price, book_discount FROM Books
            WHERE isbn = book_isbn;
            
            new_price := price-book_discount;
            RETURN new_price;
        END;
        
    FUNCTION get_price_after_tax(book_isbn VARCHAR2) 
        RETURN NUMBER
        IS price NUMBER;
        BEGIN
            price := price_after_discount(book_isbn)*1.15;
            RETURN price;
        END;
        
-- ASSIGNMENT3 --
--- QUESTION 1 ---
    FUNCTION book_purchasers(book_isbn VARCHAR2)
    RETURN customer_array_type IS customer_array customer_array_type;
    BEGIN
        SELECT customer# BULK COLLECT INTO customer_array FROM Orders
        INNER JOIN OrderItems USING(order#)
        WHERE isbn = book_isbn;
        RETURN customer_array;    
    END;
    
--- QUESTION 2 ---
    PROCEDURE show_purchases AS
        CURSOR c1 IS SELECT title, isbn FROM Books;
        c_fname VARCHAR2(50);
        c_lname VARCHAR2(50);
        c_names VARCHAR2(1000);
        b_title VARCHAR2(50);
        number_array book_store.customer_array_type;
    BEGIN
        FOR arow IN c1
        LOOP
            number_array := book_store.book_purchasers(arow.isbn);
            b_title := arow.title;
            FOR i IN 1..number_array.COUNT
            LOOP
                SELECT firstname, lastname INTO c_fname, c_lname FROM Customers
                WHERE customer# = number_array(i);
                c_names := c_names ||' '|| c_fname ||' '|| c_lname || ',';
            END LOOP;
            dbms_output.put_line(arow.isbn ||': '|| b_title ||': '|| c_names);
            c_names := NULL;
        END LOOP;
    END;
END book_store;

/

DECLARE
book_1_isbn VARCHAR2(100);
book_2_isbn VARCHAR2(100);
book_1_finalprice NUMBER(5,2);
book_2_finalprice NUMBER(5,2);
BEGIN
    SELECT isbn INTO book_1_isbn FROM Books
    WHERE title = 'BUILDING A CAR WITH TOOTHPICKS';
    
    SELECT isbn INTO book_2_isbn FROM Books
    WHERE title = 'HOLY GRAIL OF ORACLE';
    book_1_finalprice := book_store.get_price_after_tax(book_1_isbn);
    book_2_finalprice := book_store.get_price_after_tax(book_2_isbn);
    
    dbms_output.put_line(book_1_finalprice);
    dbms_output.put_line(book_2_finalprice);
END;

/
-- TESTING QUESTION 1 --
DECLARE
    cus_array book_store.customer_array_type;
BEGIN
    cus_array := book_store.book_purchasers('0401140733');
    FOR i IN 1..cus_array.COUNT
    LOOP
        dbms_output.put_line(cus_array(i));
    END LOOP;
END;

/
-- TESTING QUESTION 2 --
EXECUTE book_store.show_purchases();
