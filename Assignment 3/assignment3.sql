DROP PACKAGE BOOK_STORE;

CREATE OR REPLACE PACKAGE book_store AS
    
    FUNCTION get_price_after_tax(book_isbn VARCHAR2) RETURN NUMBER;
    TYPE customer_array_type IS VARRAY(100) OF NUMBER;
    FUNCTION book_purchasers(book_isbn VARCHAR2) RETURN customer_array_type;
    PROCEDURE show_purchases;
        
END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn VARCHAR2)
    RETURN NUMBER IS
        base_price      NUMBER(5,2);
        book_discount   NUMBER(4,2);
        final_price     NUMBER(5,2);
        BEGIN
            SELECT
                discount INTO book_discount
            FROM
                books
            WHERE
                book_isbn = isbn;
            SELECT  
                retail INTO base_price
            FROM
                books
            WHERE
                book_isbn = isbn;
            IF book_discount IS NOT NULL THEN 
                final_price:=base_price-((book_discount/100)*base_price);
            ELSE
                final_price:=base_price;
            END IF;
            RETURN(final_price);
        END;
        
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
    RETURN NUMBER IS
        discounted_price    NUMBER(5,2);
        final_price         NUMBER(5,2);
        BEGIN
            discounted_price:=price_after_discount(book_isbn);
            final_price:=discounted_price*1.15;
            RETURN(final_price);
        END;
        
    FUNCTION book_purchasers(book_isbn VARCHAR2)
    RETURN customer_array_type IS
        customerIds customer_array_type;
        BEGIN
            SELECT 
                customer# BULK COLLECT INTO customerIds
            FROM
                Orders INNER JOIN OrderItems 
                USING(order#)
            WHERE
                isbn = book_isbn;
            RETURN(customerIds);
        END;
        
    PROCEDURE show_purchases
    AS
        f_name CUSTOMERS.FIRSTNAME%TYPE;
        l_name CUSTOMERS.LASTNAME%TYPE;
        c_fullname VARCHAR2(5000);
        b_title BOOKS.TITLE%TYPE;
        myArray book_store.customer_array_type;
        BEGIN
            FOR a_row IN (SELECT isbn, title FROM BOOKS) LOOP
                myArray := book_store.book_purchasers(a_row.isbn);
                b_title := a_row.title; 
                FOR i IN 1 .. myArray.COUNT LOOP
                    SELECT 
                        FIRSTNAME, LASTNAME INTO f_name, l_name 
                    FROM 
                        CUSTOMERS
                    WHERE
                        CUSTOMER# = myArray(i);
                    c_fullname:= c_fullname || f_name || ' ' || l_name || ', ';
                END LOOP;
                DBMS_OUTPUT.PUT_LINE(a_row.isbn || ': ' || b_title || ': ' || c_fullname);
                c_fullname:=null;
            END LOOP;
        END;
       
END book_store;
/

DECLARE
BEGIN
    book_store.show_purchases;
END;