CREATE OR REPLACE PACKAGE hr_package AS
    FUNCTION get_dept_id(deptName VARCHAR2)
        RETURN NUMBER;
    FUNCTION get_city(dept_name VARCHAR2) 
        RETURN VARCHAR2;
END hr_package;
/

CREATE OR REPLACE PACKAGE BODY hr_package AS
        
    FUNCTION get_city_name(loc_id NUMBER)
    RETURN VARCHAR2 IS
        cityName VARCHAR2(200);
        BEGIN
            SELECT 
                city INTO cityName
            FROM
                hr.locations
            WHERE
                loc_id = location_id;
                RETURN(cityName);
        END;
        
    FUNCTION get_dept_id(deptName VARCHAR2) 
    RETURN NUMBER IS
        departId NUMBER;
        BEGIN
            SELECT
                DEPARTMENT_ID INTO departId
            FROM
                hr.DEPARTMENTS
            WHERE
                DEPARTMENT_NAME = deptName;
            RETURN(departId);
            
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RETURN -1;
        END;
        
    FUNCTION get_city(dept_name VARCHAR2)
    RETURN VARCHAR2 IS
        city_name VARCHAR2(200);
        dept_id NUMBER;
        loc_id NUMBER;
        BEGIN 
            dept_id:=get_dept_id(dept_name);
            SELECT 
                location_id INTO loc_id 
            FROM
                hr.departments
            WHERE 
                department_id = dept_id;
            city_name:=get_city_name(loc_id);
            RETURN(city_name);
        END;
        
    PROCEDURE add_dept_manager(dept_name HRDepartments.department_name%TYPE, man_id HRDepartments.manager_id%TYPE, dept_id HRDepartments.department_id%TYPE)
    AS
        BEGIN
            SELECT 
                department_id INTO dept_id
            FROM
                HRDepartments
            WHERE
                dept_name = department_name;
             
            UPDATE HRDEPARTMENTS SET department_id = dept_id, department = dept_name, manager_id = man_id, location_id = null;
            DBMS_OUTPUT.PUT_LINE(dept_id);
        END;
    
END hr_package;
/

DROP TABLE HRDepartments CASCADE CONSTRAINTS;
CREATE TABLE HRDepartments AS SELECT * FROM hr.departments;

DECLARE
newDepId NUMBER;
BEGIN
    newDepId:= hr_package.get_dept_id('Marketing');
    DBMS_OUTPUT.PUT_LINE(newDepId);
END; 
/